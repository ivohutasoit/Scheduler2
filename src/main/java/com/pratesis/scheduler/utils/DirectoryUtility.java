package com.pratesis.scheduler.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Utility for directory
 *
 * @author Raja Yudha Pratama Sihombing
 * @since 2
 * @version 2.0.0.20141126
 */
public class DirectoryUtility {

    /**
     *
     * @param path
     * @return
     */
    public static boolean createFolder(String path) {
        File file = new File(path);
        if (!file.exists()) {
            return file.mkdirs();
        }
        return true;
    }

    /**
     *
     * @param filepath
     * @param data
     * @throws java.lang.Exception
     */
    public static void write(String filepath, String data) throws Exception {
        try {
            File file = new File(filepath);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fileWritter = new FileWriter(file.getAbsoluteFile(), true);
            try (BufferedWriter bufferWritter = new BufferedWriter(fileWritter)) {
                bufferWritter.write(data);
            }

        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
