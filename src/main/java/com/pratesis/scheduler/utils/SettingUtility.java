package com.pratesis.scheduler.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility for setting application
 * @author Raja Yudha Pratama Sihombing
 * @since 2
 * @version 2.0.0.20141125
 */
public class SettingUtility {
    /**
     * 
     */
    public static String path;

    /**
     * 
     */
    static {
        path = System.getProperty("user.dir")
                + File.separator + "setting.properties";
    }
    
    /**
     *
     * @return
     */
    public static boolean settingExist() {
        return new File(path).exists();
    }

    /**
     *
     * @return
     */
    private static boolean createSetting() {
        if (!settingExist()) {
            File file = new File(path);
            try {
                return file.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(SettingUtility.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    /**
     *
     * @param setting
     */
    public static void writeSetting(Map<String, Object> setting) {
        Properties properties = new Properties();
        OutputStream os = null;
        try {            
            os = new FileOutputStream(path);

            // set the properties value
            for (Map.Entry<String, Object> entrySet : setting.entrySet()) 
                properties.setProperty(entrySet.getKey(), entrySet.getValue().toString());

            // save properties to project root folder
            properties.store(os, null);

        } catch (IOException ioex) {
            Logger.getLogger(SettingUtility.class.getName()).log(Level.SEVERE, null, ioex);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException ex) {
                    Logger.getLogger(SettingUtility.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    /**
     * 
     * @return 
     */
    public static Map<String, Object> readSetting() {
        Properties properties = new Properties();
        InputStream is = null;
        Map<String, Object> setting = new HashMap<String, Object>();
        try {
            is = new FileInputStream(path);
            
            properties.load(is);
            for (Map.Entry<Object, Object> entrySet : properties.entrySet()) 
                setting.put(entrySet.getKey().toString().trim(), entrySet.getValue());
            
        } catch (IOException ioex) {
            Logger.getLogger(SettingUtility.class.getName()).log(Level.SEVERE, null, ioex);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex) {
                    Logger.getLogger(SettingUtility.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return setting;
    }
}
