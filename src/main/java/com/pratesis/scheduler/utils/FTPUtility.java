package com.pratesis.scheduler.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ResourceBundle;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

/**
 * Utility for connecting to FTP server
 *
 * @author Raja Yudha Pratama Sihombing
 * @since 2
 * @version 2.0.0.20141125
 */
public class FTPUtility {

    private static ResourceBundle bundle = ResourceBundle.getBundle("global");

    private String host;
    private int port;
    private String username;
    private String password;

    private FTPClient ftpClient;
    private int replyCode;

    private InputStream stream;

    public FTPUtility() {
    }

    /**
     *
     * @param host
     * @param port
     * @param username
     * @param password
     */
    public FTPUtility(String host, int port, String username, String password) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        ftpClient = new FTPClient();
    }

    /**
     *
     * 
     * @return @throws Exception
     */
    public boolean connect() throws Exception {
        boolean isConnect = false;
        try {
            if (ftpClient.isConnected()) {
                disconnect();
            }
            ftpClient.connect(host, port);
            replyCode = ftpClient.getReplyCode();

            if (!FTPReply.isPositiveCompletion(replyCode)) {
                throw new RuntimeException(bundle.getString("connection.refused"));
            }

            boolean logged = ftpClient.login(username, password);
            if (!logged) {
                ftpClient.disconnect();
                throw new RuntimeException(bundle.getString("invalid.login"));
            }
            // Delete @ 26 November 2014
            //ftpClient.enterRemotePassiveMode();

            isConnect = true;

        } catch (IOException ioex) {
            throw new RuntimeException(ioex.getLocalizedMessage());
        }
        return isConnect;
    }

    /**
     *
     * @return @throws Exception
     */
    public boolean disconnect() throws Exception {
        boolean isConnect = true;
        if (ftpClient.isConnected()) {
            try {
                if (!ftpClient.logout()) {
                    throw new Exception(bundle.getString("unable.logout"));
                }
                ftpClient.disconnect();
                isConnect = false;
            } catch (IOException ex) {
                throw new Exception(ex.getLocalizedMessage());
            }
        }
        return isConnect;
    }

    /**
     *
     * @return
     * @throws Exception
     */
    public FTPFile[] getFiles() throws Exception {
        FTPFile[] files = null;
        try {
//            if (!path.isEmpty()) {
//                files = ftpClient.listFiles(path);
//            } else {
//                files = ftpClient.listFiles();
//            }
            files = ftpClient.listFiles();
        } catch (IOException ex) {
            throw new RuntimeException(ex.getLocalizedMessage());
        }
        return files;
    }

    /**
     *
     * @param path
     * @return
     * @throws Exception
     */
    public long getFileSize(String path) throws Exception {
        try {
            FTPFile file = ftpClient.mlistFile(path);
            if (file == null) {
                throw new RuntimeException(String.format(bundle.getString("file.not.exist"), path));
            }
            return file.getSize();
        } catch (IOException ex) {
            throw new RuntimeException(ex.getLocalizedMessage());
        }
    }

    /**
     *
     * @throws IOException
     */
    public void finish() throws IOException {
        stream.close();
        ftpClient.completePendingCommand();
    }

    /**
     *
     * @param path
     * @throws Exception
     */
    public void get(String path) throws Exception {
        try {

            boolean success = ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            if (!success) {
                throw new RuntimeException(bundle.getString("cannot.set.binary"));
            }

            stream = ftpClient.retrieveFileStream(path);

            if (stream == null) {
                throw new RuntimeException(String.format("%s %s", bundle.getString("cannot.open.stream"),
                        String.format(bundle.getString("file.not.exist"), path)));
            }
        } catch (IOException ex) {
            throw new RuntimeException(ex.getLocalizedMessage());
        }
    }

    /**
     *
     * @param path
     * @throws Exception
     */
    public void delete(String path) throws Exception {
        try {
            boolean success = ftpClient.deleteFile(path);
            if (!success) {
                throw new RuntimeException(String.format(bundle.getString("cannot.delete.file"), path));
            }
        } catch (IOException ex) {
            throw new RuntimeException(ex.getLocalizedMessage());
        }
    }

    /**
     *
     * @return
     */
    public InputStream getStream() {
        return stream;
    }
}
