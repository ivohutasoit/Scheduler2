package com.pratesis.scheduler.utils;

/**
 *
 * @author Raja Yudha Pratama Sihombing
 * @since 2
 * @version 2.0.0
 */
public enum SchedulerEnum {
    
    HOST("host"),
    PORT("port"),
    USERNAME("username"),
    PASSWORD("password"),
    SAVETO("saveto"),
    INTERVAL("interval"),
    DELETE("delete");
    
    private final String value;
    
    private SchedulerEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
    
    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
